import os
import sys
import pandas as pd
from sklearn.model_selection import train_test_split, StratifiedKFold
from catboost import CatBoostClassifier, Pool


def train_catboost(input_path: str,
                   output_path: str,
                   n_folds: int = 10,
                   verbosity: int = 0,
                   early_stopping_rounds: int = 50,
                   random_state: int = 42):
    if not os.path.exists(input_path):
        print(f"File is not loaded.")
        return

    df = pd.read_csv(input_path)
    cat_features = ['sex', 'cp', 'fbs', 'restecg', 'exang']

    X, y = df.drop(['target'], axis=1), df['target']

    X_train, X_test, y_train, y_test = \
        train_test_split(X,
                         y,
                         test_size=0.2,
                         shuffle=True,
                         stratify=y,
                         random_state=random_state)
    
    # Training
    kf = StratifiedKFold(n_splits=n_folds,
                         shuffle=True, 
                         random_state=random_state)
    catboost_params = {
        'iterations': 1000,
        'learning_rate': 0.1,
        'depth': 5,
        'cat_features': cat_features,
        'loss_function': 'Logloss',
        'eval_metric': 'F1',
        'l2_leaf_reg': 2.5,
        'min_data_in_leaf': 3,
        'subsample': 0.7,
        'colsample_bylevel': 0.8,
        'max_bin': 300,
        'one_hot_max_size': 30,
        'random_state': random_state,
        'thread_count': -1
    }
    models = []
    for fold_num, (train_index, test_index) in enumerate(kf.split(X=X_train,
                                                                  y=y_train)):
        print(f"Training, fold {fold_num}")

        X_val_train, X_val_test = \
            X_train.iloc[train_index], X_train.iloc[test_index]
        y_val_train, y_val_test = \
            y_train.iloc[train_index], y_train.iloc[test_index]
        train_dataset = Pool(data=X_val_train,
                             label=y_val_train,
                             cat_features=cat_features)
        eval_dataset = Pool(data=X_val_test,
                            label=y_val_test,
                            cat_features=cat_features)

        estimator = CatBoostClassifier(**catboost_params)
        estimator.fit(train_dataset,
                    eval_set=eval_dataset,
                    early_stopping_rounds=early_stopping_rounds,
                    verbose=verbosity,
                    use_best_model=True,
                    plot=False
                    )

        print(f"F1: {round(estimator.best_score_['validation']['F1'], 5)}\n")
        models.append({'model': estimator,
                       'score': estimator.best_score_['validation']['F1']})

    best_cb_model = max(models, key=lambda x: x['score'])['model']
    print(f"Best model F1: ",
          f"{round(best_cb_model.best_score_['validation']['F1'], 5)}")
    
    # Save model
    best_cb_model.save_model(output_path, format="cbm")


if __name__ == "__main__":
    train_catboost(*sys.argv[1:])
