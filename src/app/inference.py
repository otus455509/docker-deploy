import os
import sys
import uvicorn
import pandas as pd
import numpy as np
from fastapi import FastAPI
from pydantic import BaseModel
from catboost import CatBoostClassifier, Pool
from cb_model import train_catboost


class InputData(BaseModel):
    age: int = 52
    sex: int = 1
    cp: int = 0
    trestbps: int = 125
    chol: int = 212
    fbs: int = 0
    restecg: int = 1
    thalach: int = 168
    exang: int = 0
    oldpeak: float = 1.0
    slope: int = 2
    ca: int = 2
    thal: int = 3

app = FastAPI()

@app.get("/")
def index():
    return_html = """
        <html>
            <head>
                <title>Heart Disease</title>
            </head>
            <body>
                <h1>Catboost model!</h1>
            </body>
        </html>
    """
    return return_html

@app.post('/predict')
def predict(input_data: InputData):
    if not os.path.exists(f'models/heart.cbm'):
        train_catboost('data/heart.csv', 'models/heart.cbm')

    model = CatBoostClassifier()
    model.load_model('models/heart.cbm')

    data = pd.DataFrame(dict(input_data), index=[0])
    prediction = model.predict(data)
    return {"prediction": int(prediction[0])}

if __name__ == "__main__":
    uvicorn.run(app, host='0.0.0.0', port=5002)
